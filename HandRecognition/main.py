import cv2
import mediapipe as mp

cap = cv2.VideoCapture(0)
mp_hand = mp.solutions.hands
mp_draw = mp.solutions.drawing_utils

hands = mp_hand.Hands()

while cap.isOpened():
    success, img = cap.read()

    if not success:
        print("Camera send empty frame")
        SystemExit(-1)

    img.flags.writeable = True
    img = cv2.cvtColor(cv2.flip(img, 1), cv2.COLOR_BGR2RGB)
    results = hands.process(img)

    # We will store pixels in dict where key will be $HandID_$FingerID
    # Value Left_0 means left wirst
    hands_cords = {}
    if results.multi_hand_landmarks:
        img_heigh, img_weigth = img.shape[:2]

        for id, hnd_ldmrks in enumerate(results.multi_hand_landmarks):
            mp_draw.draw_landmarks(img, hnd_ldmrks, mp_hand.HAND_CONNECTIONS)
            for finger, lm in enumerate(hnd_ldmrks.landmark):
                for ht_id, hand_type in enumerate(results.multi_handedness):
                    if id == ht_id:
                        label = hand_type.classification[0].label
                        hands_cords[f"{label}_{finger}"] = {
                            "x": int(img_weigth * lm.x),
                            "y": int(img_heigh * lm.y),
                        }

        # Ploting type of hands in red rectangle
        if hands_cords.get("Left_4"):
            lft_wrst = (
                hands_cords.get("Left_4")["x"],
                hands_cords.get("Left_4")["y"],
            )
            lft_middle_fngr = (
                hands_cords.get("Left_20")["x"],
                hands_cords.get("Left_20")["y"],
            )
            red_color = (255, 0, 0)
            blck_clr = (0, 0, 0)
            fnt_type = cv2.FONT_ITALIC
            img = cv2.rectangle(img, lft_wrst, lft_middle_fngr, red_color, 2)
            img = cv2.putText(img, "Left", lft_wrst, fnt_type, 1, blck_clr, 3)
        if hands_cords.get("Right_4"):
            lft_wrst = (
                hands_cords.get("Right_4")["x"],
                hands_cords.get("Right_4")["y"],
            )
            lft_middle_fngr = (
                hands_cords.get("Right_20")["x"],
                hands_cords.get("Right_20")["y"],
            )
            red_color = (255, 0, 0)
            blck_clr = (0, 0, 0)
            fnt_type = cv2.FONT_ITALIC
            img = cv2.rectangle(img, lft_wrst, lft_middle_fngr, red_color, 2)
            img = cv2.putText(img, "Right", lft_wrst, fnt_type, 1, blck_clr, 3)

    # Camera change left and right side
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imshow("Video from camera", img)
    if cv2.waitKey(5) & 0xFF == 27:
        break
cap.release()
cv2.destroyAllWindows()
